//array is collection of value
// object is the collection of key value pair
// key value pair are called property
let info = {
    name:"nitan", //here name = key , "nitan" = value , name:"nitan" = property
    age: 30,
    isMarried: false,
}
// get value
console.log(info);
console.log(info.name);
console.log(info.age);
console.log(info.isMarried);  

//change value

 info.age = 31,
 info.isMarried = true,
console.log(info); 

//delete value

delete info.isMarried
console.log(info);

