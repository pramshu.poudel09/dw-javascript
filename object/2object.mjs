let info = {
    name: "nitan",
    favFruits : ["mango", "apple"],
    location : {
        country : "nepal",
        province: "bagmati",
        city: "kathamndu",
    },
    age: () => {
        console.log("i am 23");
    }
}
console.log(info.name);
console.log(info.favFruits);
console.log(info.favFruits[1]);
console.log(info.location);
console.log(info.location.country);
info.age()