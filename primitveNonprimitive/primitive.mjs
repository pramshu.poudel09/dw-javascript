//**primitive=> number,string, boolean, null, undefined

//memory allocation
//if let id used memory is allocated for that

let a=1;
let b=a;
let c=1;

//**non primitive=> array, object, date, error

//memory allocation
// before allocating memory it checks wether the variable is copy of another variable
// if the variable is copy of another, it will share memory

let ar1 = [1,2,3]
let ar2 = ar1
let ar3 = [1,2,3]
ar1.push(5);

console.log(ar1);
console.log(ar2);
console.log(ar3);

//===
// in case of primitive === checks if value is same [if same value =>true, else false]
// in case of non-primitive === checks memory address is same [if same memory address=>true, else false]