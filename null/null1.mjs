//*difference between null and undefined
// undefined means variable is defined but not initialize
// null means variable is defined and initialize with null
let a;
console.log(a); //undefined

a=5;
console.log(a); //defined

a=null;
console.log(a); //null
