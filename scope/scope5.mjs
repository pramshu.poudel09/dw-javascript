{
    let a = 10
    console.log(a);
    {
        a=12// if there is no variable defined in its own block, it changes value of "a" in parent block value to 12
        console.log(a);//12
    }
    console.log(a);//12
}
/* 
parent 
a=12

child

 */