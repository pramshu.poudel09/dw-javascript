{//parent
    let a = 1
    {//child
        let a = 10
        console.log(a);
    }
    console.log(a);
}
/* 
a variable can not be redefined in same block
but we can redefine same variable in different block 
*/
