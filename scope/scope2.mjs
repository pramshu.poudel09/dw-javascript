{//parent block(p1)
    let a = 1;
    {//children block(c1)
        console.log(a);
    }
    console.log(a);
}

/*when a variable is called
>first it search in its own block
>if variable is not find in that block then variable will be searched in its parent bloc
 */
