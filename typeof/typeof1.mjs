let a = 9;
//primitive
console.log(typeof "a")//string
console.log(typeof 1)//number
console.log(typeof false)//boolean
console.log(typeof a)//number

//non-primitive(for all non-primitive typeof is object [i.e in typeof array = object, object = object ]) 

console.log(typeof [1,3]);
console.log(typeof {name:"nitan"});
