/*  make a arrow function that takes a input as number  that perform
	if age [upto 17],  retrun your ticket is free
	if age[18 to 25 ], return  your ticket cost 100
	if age[>26],  return your ticket cost 200
	if age===26 return your ticket is 150
 */

let ticket = (age)=>{
    if (age <= 17) {
        return "Your ticket is free";
      } 
      else if (age >= 18 && age <= 25) {
        return "Your ticket costs 100";
      }
      else if (age > 26) {
        return "Your ticket costs 200";
      } 
      else if (age === 26) {
        return "Your ticket costs 150";
      }  
}
let _ticket = ticket(29)
console.log(_ticket)