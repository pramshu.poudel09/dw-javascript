/* make a arrow function that takes one input as number and return "category1" for number range from 1 to10,  return "category2" 
 for number range from 11 to 20, return "category3" for number range form 21 to 30 */

 let nums = (number)=>{
    if (number >= 1 && number <= 10){
        return "category 1"
    }
    else if (number >= 11 && number <= 20) {
        return "category2";
      } 
      else if (number >= 21 && number <= 30) {
        return "category3";
      }
}
let _nums = nums(21)
console.log(_nums)
