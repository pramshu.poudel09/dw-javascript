/* 
call stack :
it runs code inside it

event loop : 
it is a mediator which continuously which constantly monitor call stack and memory queue
if call stack is empty it push the function from memory queue to call stack

why memory queue?
because if there only memory, there is a confusion on which function to send to call stack to run
but when there is memory queue, function gets in queue to go to call stack for execution 
*/