let sum = () => {
    return 10;
}
let s = sum();
console.log(s);

// Function without return call normally sum()
// Function with return call by storing in a variable let s = sum()