let ar1 = ["a","b","c"]
let ar2 = [1,2,3]

let ar = [...ar1,...ar2]// Spread operator are the wrapper opener
console.log(ar);

let ar3 = [3,4,...ar1,10,...ar2]
console.log(ar3);

let ar4 = [...ar2,...ar1]
console.log(ar4);