//**INTERVIEW QUESTION** 
let a = NaN
console.log(a === NaN);

//We can not check Nan from this way

//CORRECT WAY=> to check whether a variable is NaN or not we use isNaN() function
console.log(isNaN(a));