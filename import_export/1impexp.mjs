/* 
   1. export by name
      => a file has many export by name, i.e many export by name can be used in a single file 
      =>need curly braces while import
      => while import you should use same name which is used while export

   2. export by default
      => a file has only one export by name, i.e only one export by default should be used in a file
      => does not need curly braces while import
      => while import you can use any name
      
      */
      
//1. export by name   
export let name = "nitan"
export let age =29
export let address = gagalphedi


//2. export by default
let education = "phd";
export default education;
