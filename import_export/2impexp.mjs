//export by name
import {name} from "./1impexp.mjs"
console.log(name);

import {age} from "./folder/1fold.mjs"
console.log(age);

import { isMarried } from "./folder1/2fold.mjs";
console.log(isMarried);

import { name,age, address } from "./1impexp.mjs";
console.log(name,age, address);

/* ********************************************************************************************************************************************** */

//export by default
import education from "./1impexp.mjs";
console.log(education);

import education, { name,age, address }from "./1impexp.mjs";
console.log(education);
console.log(name,age, address); // both export by name and default can be used together

import abc, { name,age, address }from "./1impexp.mjs";
console.log(abc);
console.log(name);
console.log(age);
console.log(address);