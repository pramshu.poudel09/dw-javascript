let names = ["ram", "roshan", "ramesh", 27 , false]
//index     [ 0,        1,        2,    3,    4]
console.log(names)
console.log(names[2]) //indexing
console.log(names[4]) //indexing
names[2] = "hari" // replacing element in array
console.log(names) 

//array is used to store multiple value of same or different data type
//array has indexing , indexing starts from 0
//indeing is used to get or change particular element of array
